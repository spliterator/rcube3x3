package com.butilov.cube

enum class CubeOperation {
    R, L, F, B, U, D, S, M, E,
    Rh, Lh, Fh, Bh, Uh, Dh, Sh, Mh, Eh,
    R2, L2, F2, B2, U2, D2, S2, M2, E2,
    y, yh, z, zh, x, xh,
    // Rw, Lw, Fw, Bw, Uw, Dw,
    // Rwh, Lwh, Fwh, Bwh, Uwh, Dwh,
}
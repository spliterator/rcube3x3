package com.butilov.cube

class CoordinateRotator {

    /**
     * ( Y,  Z)
     * ( 1, -1) -> ( 1,  1) z*=-1
     * ( 1,  0) -> ( 0,  1) y<->z
     * ( 1,  1) -> (-1,  1) y*=-1
     * ( 0, -1) -> ( 1,  0) y<->-z
     * ( 0,  1) -> (-1,  0) y<->-z
     * (-1, -1) -> ( 1, -1) y*=-1
     * (-1,  0) -> ( 0, -1) y<->z
     * (-1,  1) -> (-1, -1) z*=-1
     */
    fun aroundX90(coordinate: Coordinate) {
        with(coordinate) {
            when {
                z == 0 -> {
                    z = y
                    y = 0
                }
                y == 0 -> {
                    y = -z
                    z = 0
                }
                z == y -> y *= -1
                z == -y -> z *= -1
            }
        }
    }

    fun aroundXminus90(coordinate: Coordinate) {
        with(coordinate) {
            when {
                z == 0 -> {
                    z = -y
                    y = 0
                }
                y == 0 -> {
                    y = z
                    z = 0
                }
                z == y -> z *= -1
                z == -y -> y *= -1
            }
        }
    }

    fun aroundX180(coordinate: Coordinate) = with(coordinate) {
        y *= -1
        z *= -1
    }


    fun aroundY90(coordinate: Coordinate) {
        with(coordinate) {
            when {
                z == 0 -> {
                    z = -x
                    x = 0
                }
                x == 0 -> {
                    x = z
                    z = 0
                }
                z == x -> z *= -1
                z == -x -> x *= -1
            }
        }
    }

    fun aroundYminus90(coordinate: Coordinate) {
        with(coordinate) {
            when {
                z == 0 -> {
                    z = x
                    x = 0
                }
                x == 0 -> {
                    x = -z
                    z = 0
                }
                z == x -> x *= -1
                z == -x -> z *= -1
            }
        }
    }

    fun aroundY180(coordinate: Coordinate) = with(coordinate) {
        x *= -1
        z *= -1
    }

    fun aroundZ90(coordinate: Coordinate) {
        with(coordinate) {
            when {
                x == 0 -> {
                    x = y
                    y = 0
                }
                y == 0 -> {
                    y = -x
                    x = 0
                }
                x == y -> y *= -1
                x == -y -> x *= -1
            }
        }
    }

    fun aroundZminus90(coordinate: Coordinate) {
        with(coordinate) {
            when {
                x == 0 -> {
                    x = -y
                    y = 0
                }
                y == 0 -> {
                    y = x
                    x = 0
                }
                x == y -> x *= -1
                x == -y -> y *= -1
            }
        }
    }

    fun aroundZ180(coordinate: Coordinate) = with(coordinate) {
        x *= -1
        y *= -1
    }

}
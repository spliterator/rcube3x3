package com.butilov.cube

import org.junit.Test

class Cube3x3InitTest {

    private val cube = Cube3x3()
    private val fringeSize = 9

    @Test
    fun `check upper layer`() {
        val upper = cube.getUpperLayer()

        assert(upper.size == fringeSize)
        upper.forEach { assert(it.yColor == Color.Yellow) }
    }

    @Test
    fun `check down layer`() {
        val down = cube.getDownLayer()

        assert(down.size == fringeSize)
        down.forEach { assert(it.yColor == Color.White) }
    }


    @Test
    fun `check left layer`() {
        val left = cube.getLeftLayer()

        assert(left.size == fringeSize)
        left.forEach { assert(it.zColor == Color.Green) }
    }

    @Test
    fun `check right layer`() {
        val right = cube.getRightLayer()

        assert(right.size == fringeSize)
        right.forEach { assert(it.zColor == Color.Blue) }
    }

    @Test
    fun `check front layer`() {
        val front = cube.getFrontLayer()

        assert(front.size == fringeSize)
        front.forEach { assert(it.xColor == Color.Orange) }
    }

    @Test
    fun `check back layer`() {
        val back = cube.getBackLayer()

        assert(back.size == fringeSize)
        back.forEach { assert(it.xColor == Color.Red) }
    }

}
